FROM docker:18.09.3

ENV NF_SERVER=https://api.nodeferret.com

RUN apk add dbus curl bash util-linux

COPY . /opt/nodeferret/

CMD /bin/bash -c "/opt/nodeferret/start.sh && crond -f"