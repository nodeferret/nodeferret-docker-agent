#!/bin/bash
#
# NodeFerret Docker Agent (nodeferret.com)

# Flags for experimental features
zfsflag='false'
dockerflag='false'
node='false'
while getopts ':zdn:' flag; do
  case "${flag}" in
    z) zfsflag='true' ;;
    d) dockerflag='true' ;;
    n) node=$OPTARG ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      ;;
  esac
done


if [ "${node}" = 'false' ] ; then
    echo "'node' is required; please supply with -n";
    exit 1;
fi


NODE=${node}
API_KEY=${API_KEY}
SERVER=${NF_SERVER}
SERVER_PATH=${SERVER}/v1/raw/


# Fetch System info
eval `docker info --format 'HOST_ID={{json .ID}}
HOST_NAME={{json .Name}}
HOST_OS={{json .OperatingSystem}}
HOST_OSTYPE={{json .OSType}}
HOST_KERNEL={{json .KernelVersion}}
HOST_ARCH={{json .Architecture}}
HOST_NCPU={{json .NCPU}}
HOST_MEMTOTAL={{json .MemTotal}}'`

os_name=${HOST_OS}
os_version=""

hostname=${HOST_NAME}
kernel_name=${HOST_OSTYPE}
kernel_release=${HOST_KERNEL}
cpu_architecture=${HOST_ARCH}
ip_list=$( ip -o addr | awk '!/^[0-9]*: ?lo|link\/ether/ {gsub("/", " ") ; print $2, $4}' | sed -e ':a' -e 'N' -e '$!ba' -e 's/\n/\\n/g' )
uptime=$( cat /host/proc/uptime | awk '{print $1}' ) # uptime -s ?

process_count=$( ps aux | wc -l )
file_handles=$( sysctl fs.file-nr | sed 's/\t/  /g' ) # result contains tabs

# Fetch bulky data to process on server
RAW_CPU_CONFIG=$( lscpu | grep -v Flags | sed -e ':a' -e 'N' -e '$!ba' -e 's/\n/\\n/g' )
RAW_CPU_DATA=$( cat /host/proc/stat | grep cpu | sed -e ':a' -e 'N' -e '$!ba' -e 's/\n/\\n/g' )
RAW_MEMORY_DATA=$( cat /host/proc/meminfo | head -n 20 | sed -e ':a' -e 'N' -e '$!ba' -e 's/\n/\\n/g' )

RAW_DISK_SIZE_DATA=$( df -Tk | grep -Ee '\S+\s+(ext[234]|vfat|xfs|simfs|fuseblk)' | grep -v -Ee '(^/dev/zd|\S+\s+zfs)' | sed -e 's/host$//g' -Ee 's/ \/host/ /g' | sed -e ':a' -e 'N' -e '$!ba' -e 's/\n/\\n/g' ) # 1k blocks
RAW_DISK_INODES_DATA=$( df -Ti | grep -Ee '\S+\s+(ext[234]|vfat|xfs|simfs|fuseblk)' | grep -v -Ee '(^/dev/zd|\S+\s+zfs)' | sed -e 's/host$//g' -Ee 's/ \/host/ /g' | sed -e ':a' -e 'N' -e '$!ba' -e 's/\n/\\n/g' ) # 1k blocks

RAW_NETWORK_DATA=$( cat /host/proc/net/dev | grep -v -e 'Inter' -e 'face' | sed -e ':a' -e 'N' -e '$!ba' -e 's/\n/\\n/g' ) # Exclude first 2 lines

# TODO: Consider recompiling ps, or pulling data ourselves
RAW_PROCESS_CPU_DATA=$( ps aux | grep -v COMMAND | awk '{arr[$11]+=$3} ; END {for (key in arr) print arr[key],key}' | sort -rnk1 | head -n 10 | sed -e ':a' -e 'N' -e '$!ba' -e 's/\n/\\n/g' ) # Sort by Cpu Usage
RAW_PROCESS_RAM_DATA=$( ps aux | grep -v COMMAND | awk '{arr[$11]+=$4} ; END {for (key in arr) print arr[key],key}' | sort -rnk1 | head -n 10 | sed -e ':a' -e 'N' -e '$!ba' -e 's/\n/\\n/g' ) # Sort by Ram Usage

if [ "${zfsflag}" = 'true' ] ; then
    RAW_ZFS_DATA=$( zpool list | grep -v -Ee '^NAME' | sed -e ':a' -e 'N' -e '$!ba' -e 's/\n/\\n/g'  )
fi

if [ "${dockerflag}" = 'true' ] ; then
    RAW_DOCKER_DATA=$( docker stats --no-stream --no-trunc | sed -e ':a' -e 'N' -e '$!ba' -e 's/\n/\\n/g' )
fi

CONTENT=$(cat << END
{
    "node": ${NODE},
    "api_key": "${API_KEY}",

    "hostname": "${hostname}",
    "os_name": "${os_name}",
    "os_version": "${os_version}",
    "kernel_name": "${kernel_name}",
    "kernel_release": "${kernel_release}",
    "cpu_architecture": "${cpu_architecture}",

    "uptime": "${uptime}",
    "process_count": "${process_count}",
    "file_handles": "${file_handles}",
    "ip_list": "${ip_list}",

    "cpu_config": "${RAW_CPU_CONFIG}",
    "cpu": "${RAW_CPU_DATA}",
    "ram": "${RAW_MEMORY_DATA}",
    "disk": "${RAW_DISK_SIZE_DATA}",
    "inode": "${RAW_DISK_INODES_DATA}",
    "network": "${RAW_NETWORK_DATA}",
    "process_cpu": "${RAW_PROCESS_CPU_DATA}",
    "process_ram": "${RAW_PROCESS_RAM_DATA}",
    "zfs": "${RAW_ZFS_DATA}",
    "docker": "${RAW_DOCKER_DATA}"
}
END
)

curl -si -H "Content-Type: application/json" -X POST --data "${CONTENT}" ${SERVER_PATH}

printf '\n'