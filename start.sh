#!/bin/bash

if [ -z "${NF_SERVER}" ]; then
    echo "NF_SERVER environment variable is required"
    exit 1
fi

if [ -z "${API_KEY}" ]; then
    echo "API_KEY environment variable is required"
    exit 1
fi


# Get or Create machine identifier
# Use /sys/class/dmi/id/product_uuid, fallback of /host/machine-id (mounted from /etc/machine-id on host)
# Note: /var/lib/dbus/machine-id is install specific (all containers from a docker image would have the same id)
# Consider: docker info --format '{{ .ID}}'
if [ -f /sys/class/dmi/id/product_uuid ]; then
    NODE_IDENTIFIER=$( cat /sys/class/dmi/id/product_uuid )
elif [ -f /host/machine-id ]; then
    NODE_IDENTIFIER=$( cat /host/machine-id )
else
    echo "Unable to determine identifier of machine"
    exit 1
fi


# Get host hostname as label
label=$(docker info --format '{{ .Name}}')

# if host hostname fails, fallback to container-name
if [ -z "${label}" ]; then
    label=$( hostname -f )
fi

# Check if token is valid
status_code=$(curl --write-out %{http_code} --silent --output /dev/null \
    ${NF_SERVER}/v1/util/get-node-id/${API_KEY}/${NODE_IDENTIFIER}/${label}/)


if [ "${status_code}" = '200' ] || [ "${status_code}" = '201' ] ; then
    echo "Authentication successful"
else
    echo "Unable to authenticate. Status: ${status_code}"
    exit 1
fi

# Get node_id from identifier
NF_NODE=$(curl -s -H "Content-Type: application/json" \
    -X GET ${NF_SERVER}/v1/util/get-node-id/${API_KEY}/${NODE_IDENTIFIER}/${label}/)


printf 'creating crontab entry...'
(crontab -l 2>/dev/null; printf "*/1 * * * * /bin/bash /opt/nodeferret/agent.sh -n ${NF_NODE} \n") | crontab -
printf 'done\n'
