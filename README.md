# nodeferret-docker-agent
**Official Docker Agent for NodeFerret**


## Supported Tags

 - `2.2`, `latest`


## How to use this Image

#### For basic functionality
```bash
docker run -d --name nf-agent \
  -v /var/run/docker.sock:/var/run/docker.sock:ro \
  -v /proc/:/host/proc/:ro \
  -e API_KEY=<TOKEN> \
  nodeferret/agent:latest
```

#### For more complete disk usage data
```bash
docker run -d --name nf-agent \
  -v /var/run/docker.sock:/var/run/docker.sock:ro \
  -v /:/host/:ro \
  -e API_KEY=<TOKEN> \
  nodeferret/agent:latest
```

Expert users can also cherry pick the volumes to mount to the image.  
All mounted block devices will be monitored.  
`/proc/` and `/var/run/docker.sock` are mandatory


## Other 

#### Rancher 2.X users

Add the following tolerations to Pod spec

```yaml
tolerations:
- key: "node-role.kubernetes.io/controlplane"
  operator: "Exists"
  effect: "NoSchedule"
- key: "node-role.kubernetes.io/etcd"
  operator: "Exists"
  effect: "NoExecute"
```